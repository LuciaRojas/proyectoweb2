<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function authenticate($email, $password){
      $query = $this->db->get_where('users', array('email' => $email, 'password' => $password));
      if ($query->result()) {
        return $query->result()[0];
      } else {
        return false;
      }
  }
  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
        public function authenticateloginadmin($email, $password){
          $query = $this->db->get_where('users', array('email' => $email, 'password' => $password,'user_type' => "Admin"));
          if ($query->result()) {
            return $query->result()[0];
          } else {
            return false;
          }
      }
    public function authenticateloginfriend($email, $password){
      $query = $this->db->get_where('users', array('email' => $email, 'password' => $password,'user_type' => "friend"));
      if ($query->result()) {
        return $query->result()[0];
      } else {
        return false;
      }
    }
  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function getByName($name){
      $query = $this->db->get_where('users', array('name' => $name));
      if ($query->result()) {
        return $query->result();
      } else {
        return false;
      }
  }
    /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function getByEmail($email){
    $query = $this->db->get_where('users', array('email' => $email));
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
}

  /**
   *  Get all users from the databas
   *
   */
  public function all(){
      $query = $this->db->get('users');
      return $query->result();
  }

  /**
   *  Get friend users from the databas
   *
   */
  public function friendlist(){
    $query = $this->db->get_where('users', array('user_type' => "friend"));
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
  }
/***
 * trae los datos de los arboles
 */
  public function treelist(){
    $query = $this->db->get('tree');
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }

  }
  /***
   * Inserta los usuario
   * arreglo de usuarios
   */
  public function insertUsuario($userw){
    $query = $this->db->insert('users', $userw);
    

  }
}