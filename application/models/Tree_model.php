<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tree_model extends CI_Model {


/**
   *  Get friend users from the databas
   *
   */
  public function friendlisttree($id_user){

    $query = $this->db->get_where('tree', array('id_user' => $id_user));
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
  }
  /*****
   * Obtiene los datos del arbol que contengan el id_user
   * @id_user= id usuario
   */
  public function friendlisttree2($id_user){

    $query = $this->db->select('*');    
    $query = $this->db->from('tree');
    $query = $this->db->join('users', 'tree.id_user = users.id_user', 'inner');
    $query = $this->db->join('especie', 'tree.id_especie = especie.id_especie', 'inner');
    $query = $this->db->where("tree.id_user", $id_user);
    $query = $this->db->get();
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function listespecie(){
    $query = $this->db->get('especie');
    return $query->result();
}

/***
 * obtiene los datos del arbol 
 */
public function treeedit($id_tree){

  $query = $this->db->get_where('tree', array('id_tree' => $id_tree));
  if ($query->result()) {
    return $query->result();
  } else {
    return false;
  }
}


/***
 * obtiene los datos del arbol 
 */
public function getphotographs($id_tree){

  $query = $this->db->get_where('photographs', array('id_tree' => $id_tree));
  if ($query->result()) {
    return $query->result();
  } else {
    return false;
  }
}

public function insertTree($tree){
  $query = $this->db->insert('tree', $tree);
  

}

public function insertpicture($picture){
  $query = $this->db->insert('photographs',$picture);
  

}
/**
 * Actualiza de los datos con los datos nuevo , con el id_tree
 * @$id_tree id del árbol
 * @$datos los datos a actualizar
 */
public function editar_tree($id_tree, $datos){
  //Se hace el where para actualizar el registro que se desea
  $query = $this->db->where('id_tree', $id_tree);
  //Se hace el update a la tabla con los datos enviados
  $query =$this->db->update('tree', $datos);
}

/**
 * elimina los datos de la Photography con el id de la fotografia 
 * @$id_Photography el id de la fotografia a eliminar 
 */
public function deletePhotography($id_Photography)
{
  $query = $this->db->delete('photographs', array('id_Photography' => $id_Photography));
}



}