<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tree extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	   /**
   *  Validate in the database that the user exists
   *
   * @param $id_user  id user
   */
	public function listtree($id_user){
		if($this->tree_model!== null){
			if($datatree['trees'] = $this->tree_model->friendlisttree2($id_user)){

				$datatree['treesQty'] = sizeof($datatree['trees']);
				$this->load->view('tree/listtree', $datatree);
			}else{
				$datatree['trees'] = null;
				$this->load->view('tree/listtree', $datatree);
			}

		}else{
			$datatree['trees'] = null;
			$this->load->view('tree/listtree', $datatree);
		}
			
		

		
	}
/**
 * trae los datos del arbol por el id del usuario y busca por el id del arbol la fotogafia
*/
	public function edittree($id_tree){

		if($this->tree_model!== null){
			//$datatree['edittree'] = $this->tree_model->treeedit($id_tree);
			
			if($datatree['edittree'] = $this->tree_model->treeedit($id_tree)){
				$datatree['treeQty'] = sizeof($datatree['edittree']);

			}else{

				$datatree['edittree'] = null;
			}
			if($datatree['especies'] = $this->tree_model->listespecie()){

				//$datatree['especies'] = $this->tree_model->listespecie();
				$datatree['especieQty'] = sizeof($datatree['especies']);
			}else{
				$datatree['especies'] = null;

			}
			if($datatree['photographs'] = $this->tree_model->getphotographs($id_tree)){
				//$datatree['photographs'] = $this->tree_model->getphotographs($id_tree);
				$datatree['photographQty'] = sizeof($datatree['photographs']);

			}else{
				$datatree['photographs'] = null;
			}
		}
	
		$this->load->view('tree/edittree',$datatree);
	

	
}


/**
 * obtiene la lista de las especies y las envia a la vista del formulario de insertar
 */
public function frminsert(){

	$datatree['especies'] = $this->tree_model->listespecie();
		$datatree['especieQty'] = sizeof($datatree['especies']);
		$this->load->view('tree/instree',$datatree);
	

	
}
/***
 * trae los datos y la foto del arbol 
 * $id_tree el id del arbol
 */
public function seeTree($id_tree){

	if($this->tree_model!== null){
			if($datatree['photographs'] = $this->tree_model->getphotographs($id_tree)){
			//$datatree['photographs'] = $this->tree_model->getphotographs($id_tree);
			$datatree['photographQty'] = sizeof($datatree['photographs']);
			$this->load->view('tree/seetree',$datatree);
			}else{
			$datatree['photographs'] = null;
			$this->load->view('tree/seetree', $datatree);
		    }
	}else{

		$datatree['photographs'] = null;
		$this->load->view('tree/seetree', $datatree);
	}

}

/**
 * revisa que los datos no esten vacios luego lo pasa el model_tree y luego  Inserta los datos del árbol como
 * el nombre del árbol
 * el monto
 * la especie
*/
public function treeinsert(){

///$this->session->user->id_user;
			date_default_timezone_set('America/Costa_Rica');
    		  $fecha= date("yy/m/d"); 
			if($this->input->post()){
			$validation_errors = [];

			if(!$this->input->post('nom_tree')){
				$validation_errors['nom_tree'] = 'is blank';
			}
			if(!$this->input->post('monto')){
				$validation_errors['monto'] = 'is blank';
			}
			if(!$this->input->post('id_especie')){
				$validation_errors['id_especie'] = 'is blank';
			}
			
			if(sizeof($validation_errors) > 0) {
				$this->session->set_flashdata('validation_errors', $validation_errors);
				redirect(site_url(['tree','frminsert']));
			}else{
				$tree = array(
					"id_user" => $this->session->user->id_user,
					"nom_tree" => $this->input->post('nom_tree'),
					"id_especie" => $this->input->post('id_especie'),
					"monto" => $this->input->post('monto'),
					"fecha" => $fecha

			
					
				);
				$inserTree = $this->tree_model->insertTree($tree);
				$this->session->set_flashdata('validation_errors', 'Se han guardado exitosamente');
				redirect(site_url(['tree','frminsert']));
			}

		
		}
		

		
	}
/***
 * revisa que los datos no esten vacios luego lo pasa el model_tree y luego  editar el árbol como agregas fotos del árbol
 */
	public function editarbolfoto(){
		$id_tree=$this->input->post('id_tree');
		$id_especie=$this->input->post('id_especie');
		$alt=$this->input->post('alt');
		$pho = $this->input->post('nom_arbol');

		$validation_errors = [];
		if(!$id_tree){
			$validation_errors['id_tree'] = 'is blank';
		}
		if(!$id_especie){
			$validation_errors['id_especie'] = 'is blank';
		}
		if(!$alt){
			$validation_errors['alt'] = 'is blank';
		}

		if(sizeof($validation_errors) > 0) {
			$this->session->set_flashdata('validation_errors', $validation_errors);
			redirect(site_url(['tree','edittree',$id_tree]));
		}else{

			$datos = array(
				"alt" => $alt,
				"id_especie" => $id_especie	
			);
				if( $pho !== "" ){
					$validation_errors['picture'] = 'is blank';
					$this-> uploadPicture($pho,$id_tree); 
				}

				$updatetree = $this->tree_model->editar_tree($id_tree,$datos);
				$this->session->set_flashdata('validation_errors', "Se guardo con exito");
				redirect(site_url(['tree','edittree',$id_tree]));

		}

}
/***
 * guarda la foto subida en la carpeta uploads y luego lo envia el model_tree para insertar la ruta de la foto
 */
	public function uploadPicture($inputName,$id_tree){
		
			$config['upload_path'] = './uploads/';
        	$config['allowed_types'] = 'gif|jpg|png|jpeg';
        	$config['max_size'] = 2048;
        	$config['max_width'] = 2024;
        	$config['max_height'] = 2008;

			$this->load->library('upload', $config);
			if($this->upload->do_upload('picture'))
			{
				date_default_timezone_set('America/Costa_Rica');
				$fecha= date("yy/m/d"); 
				
				$picture = array(
					"id_tree" => $id_tree,
					"profilePic" => 'uploads/'.$inputName,
					"fechap" => $fecha	
				);

			//	echo var_dump($picture);
				$insertpicture = $this->tree_model->insertpicture($picture);
				//$this->session->set_flashdata('validation_errors', "Se guardo con exito");
				//redirect(site_url(['tree','edittree',$id_tree]));
			
			//echo "exito";
			}
		///	else
			///{
				//$this->session->set_flashdata('validation_errors', "No se guardo");
			//	redirect(site_url(['tree','edittree',$id_tree]));
			///}			
	  
		//$target_dir = "uploads/";
		//$target_file = $target_dir . basename($fileObject["name"]);
		//$uploadOk = 0;
		//$comprobarimg=false;

		//echo "la ruta es: ".$target_file;

		/*if (move_uploaded_file($fileObject["tmp_name"], $target_file)) { 
	  
			echo $target_file;



		  $comprobarimg=true;
		  return $target_file;
		} else {
			echo 'Error';
			$comprobarimg=false;
		  return $comprobarimg;
		}*/
	  }

/**
 * Elimina la foto de la carpeta uploads y para luego pasarlo al model_tree y elimina la ruta de la base de datos de la fotografia
 * 
 */
	  public function eliminarphoto($rut,$name,$id_Photography,$id_tree){

		//echo $rut;
		//echo $name;
		//echo $id_Photography;
		$namephoto= $rut.'/'.$name;

		//unlink($upload_dir.$edit_row['userPic']);
		echo $namephoto;
		//'uploads/84252ff49cf169932047a02235aeec1e1583579083_full.png'
		unlink($namephoto);//acá le damos la direccion exacta del archivo
		$deletepicture = $this->tree_model->deletePhotography($id_Photography);
		redirect(site_url(['tree','edittree',$id_tree]));
	  }

	
	 
	 


}


