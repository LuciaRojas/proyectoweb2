<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 /**
	  * pagina principal
	  */
public function mainprin(){

	$this->load->view('user/mainprin');
}
/**
 * login del administrador
 */
	public function login()
	{
    // params
		// load data
		$this->load->view('common/head');
		$this->load->view('user/login');
		$this->load->view('common/footer');

	}
	/**
	 * login del amigo
	 */
	public function loginfriend()
	{
    // params
		// load data
		$this->load->view('common/head');
		$this->load->view('user/loginfriend');
		$this->load->view('common/footer');

	}
	/**
	 * pagina no autorizada si un amigo entra en el login del administrador
	 */
	public function noautorizado()
	{
    // params
		// load data
		$this->load->view('user/noautorizado');


	}
	/**
	 * lo de vuelve al mainprincipal
	 */
	public function volvermain()
	{
    // params
		// load data
		redirect(site_url(['user','mainprin']));


	}
/**
 * se sale de la seccion
 */
	public function logout()
	{
		$this->session->sess_destroy();
    $this->session->set_flashdata('error', 'Inicie sesión nuevamente');
		redirect(site_url(['user','mainprin']));
	}

	/**
	 * Loads the user's principalfriend friend
	 */

	public function principalfriend(){
		if($this->tree_model!== null ){
			if($this->session->has_userdata('user')){

				$this->load->view('common/head');
				//$datatree['treesfriend'] = $this->tree_model->friendlisttree2($this->session->user->id_user);
				if($datatree['treesfriend'] = $this->tree_model->friendlisttree2($this->session->user->id_user)){
					$datatree['treesQty'] = sizeof($datatree['treesfriend']);
	
					$this->load->view('common/navbar', $datatree);
		
					$this->load->view('user/principalfriend');
					$this->load->view('common/footer');

				}else{
					$datatree['treesfriend'] = null;
					$this->load->view('common/navbar', $datatree);

				}
				
				
				
			} else {
				$this->session->set_flashdata('error', 'No ha iniciado sesión');
				redirect(site_url(['user','mainprin']));
			}
		
		}else{
			$datatree['treesfriend'] = null;
					$this->load->view('common/navbar', $datatree);

		}


	}
	/**
	 * Loads the user's dashboard admin
	 */

	public function dashboard()
	{
		if($this->user_model!== null ){
			
			if($this->session->has_userdata('user')){
				$this->load->view('common/head');
				if($data['users'] = $this->user_model->friendlist()){
					$data['usersQty'] = sizeof($data['users']);
					/************************************************************************* */
					$datatree['trees'] = $this->user_model->treelist();
					$datatree['treesQty'] = sizeof($datatree['trees']);
					 $tree=count($datatree);
				/************************************************************************* */
					$data['cantidadtree']=$tree;
				$this->load->view('common/navbar', $data);
					$this->load->view('user/dashboard');
					$this->load->view('common/footer');
				}else{
					$datatree['trees'] = null;
					$data['users'] =null;
					$this->load->view('common/navbar', $datatree);
				}
				//$data['users'] = $this->user_model->friendlist();
				
				
			} else {
				$this->session->set_flashdata('error', 'No ha iniciado sesión');
				redirect(site_url(['user','mainprin']));
			}

		}else{
					$datatree['trees'] = null;
					$data['users'] =null;
					$this->load->view('common/navbar', $datatree);
		}


		
  }
/**
	 * sign_in registrarse
	 */
  public function sign_in()
  {
	$this->load->view('user/sign_in');
}

	public function register()
	{
		$this->load->view('user/register');
	}

/***
 * valida si los datos no esten en blanco 
 */
	public function save()
	{
		$validation_errors = [];

		if(!$this->input-post('name')){
			$validation_errors['name'] = 'is blank';
		}
		if(!$this->input-post('username')){
			$validation_errors['username'] = 'is blank';
		}

		if(sizeof($validation_errors) > 0) {
			$this->session->set_flashdata('validation_errors', $validation_errors);
			redirect(site_url(['user','register']));
		}
	}

	public function view($id){
		$data['user'] = $this->user_model->getById($id)[0];
		$this->load->view('user/view', $data);
	}

	public function search($name = ''){
		$data['users'] = $this->user_model->getByName($name);
		$data['usersQty'] = sizeof($data['users']);
		$this->load->view('user/list', $data);
	}

	public function list()
	{
		$data['users'] = $this->user_model->all();
		$data['usersQty'] = sizeof($data['users']);
		$this->load->view('user/list', $data);
	}
	/**
	 * obtiene la lista de amigos y lo de vuelve al vista de navbar
	 */
	public function listfriend()
	{
		$data['users'] = $this->user_model->friendlist();
		$data['usersQty'] = sizeof($data['users']);
		$this->load->view('common/navbar', $data);
	}

	/**
	 * Authenticates a user
	 */
	public function authenticate()
	{
		// get username and password
		$pass = $this->input->post('password');
		$email = $this->input->post('email');

		// check the database with that information
		$authUser = $this->user_model->authenticate($email, $pass);
		// return error or redirect to landing page
		if ($authUser) {
			$this->session->set_userdata('user', $authUser);
			redirect(site_url(['user','dashboard']));
		} else {
			$this->session->set_flashdata('error', 'Invalid username or password');
			redirect(site_url(['user','login']));
		}
	}

	/**
	 * Authenticates a user admin
	 */
	public function authenticateAdmin()
	{ 		
		// get username and password
		$pass = $this->input->post('password');
		$email = $this->input->post('email');

		// check the database with that information
		$authUser = $this->user_model->authenticateloginadmin($email, $pass);
		// return error or redirect to landing page
		if ($authUser) {
			$this->session->set_userdata('user', $authUser);
			redirect(site_url(['user','dashboard']));
		
		} else {
			$this->session->set_flashdata('error', 'Invalid username or password');
			
			redirect(site_url(['user','noautorizado']));
		}

	}
		/**
	 * Authenticates a user friend
	 */
	public function authenticatefriend()
	{ 		
		// get username and password
		$pass = $this->input->post('password');
		$email = $this->input->post('email');

		// check the database with that information
		$authUser = $this->user_model->authenticateloginfriend($email, $pass);
		// return error or redirect to landing page
		if ($authUser) {
			$this->session->set_userdata('user', $authUser);
			redirect(site_url(['user','principalfriend']));
		echo "adim";
		} else {
			$this->session->set_flashdata('error', 'Invalid username or password');
			echo "amigo";
			redirect(site_url(['user','noautorizado']));
		}

	}
	/***
	 * tipo de login si es admin o friend
	 */
	public function tipologin(){

		if($this->input->post()){
			$tipoadmin = $this->input->post('admin');
			echo $tipoadmin;
				if($tipoadmin=="Administrador"){

				redirect(site_url(['user','login']));
			}else{

				redirect(site_url(['user','loginfriend']));
			}

			
		}

	}


	/***
	 * revisa que los datos no esten vacios luego lo pasa el model_user para Inserta los datos del usuario a la hora de registrarse
	 */
	public function insertUsuario()
	{

		if($this->input->post()){
			$validation_errors = [];

			if(!$this->input->post('name')){
				$validation_errors['name'] = 'is blank';
			}
			if(!$this->input->post('last_name')){
				$validation_errors['last_name'] = 'is blank';
			}
			if(!$this->input->post('num_tel')){
				$validation_errors['num_tel'] = 'is blank';
			}
			if(!$this->input->post('email')){
				$validation_errors['email'] = 'is blank';
			}
			if(!$this->input->post('address')){
				$validation_errors['address'] = 'is blank';
			}
			if(!$this->input->post('country')){
				$validation_errors['country'] = 'is blank';
			}
			if(!$this->input->post('username')){
				$validation_errors['username'] = 'is blank';
			}
	
			if(!$this->input->post('password')){
				$validation_errors['password'] = 'is blank';
			}
	
	
			if(sizeof($validation_errors) > 0) {
				$this->session->set_flashdata('validation_errors', $validation_errors);
				redirect(site_url(['user','sign_in']));
			}else{

					///envia el correo al model_user para validar si esta, si esta no pude registrarse
					// si no esta puede registrarse 

				
					if($this->user_model->getByEmail($this->input->post('email'))){
						$this->session->set_flashdata('validation_errors', "El correo ya existe");
						redirect(site_url(['user','sign_in']));
					}else{

						$userw = array(
							"name" => $this->input->post('name'),
							"last_name" => $this->input->post('last_name'),
							"num_tel" => $this->input->post('num_tel'),
							"email" => $this->input->post('email'),
							"address" => $this->input->post('address'),
							"country" => $this->input->post('country'),
							"user" => $this->input->post('username'),
							"password" => $this->input->post('password'),
							"user_type"=>"friend"
						);
						$insertUser = $this->user_model->insertUsuario($userw);
						$this->session->set_flashdata('validation_errors', "Se han guardado exitosamente");
						redirect(site_url(['user','sign_in']));


					}

				
			}
		}
	}

}