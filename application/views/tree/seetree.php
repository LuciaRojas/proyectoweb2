<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($this->session->user->user_type=="friend"){
  $isAdmin = true;



}else{
  $isAdmin = false;

}

if(!$isAdmin){
  redirect(site_url(['user','mainprin']));

}
?>

<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/dashboard/mytree/css/style.css">
    <title>Editar</title>
  </head>
  <body>
  <div class="container">
    <div class="msg">
    <?php echo $this->session->flashdata('validation_errors');?>
    </div>
    <nav class="">
      <a  href="<?php echo site_url(['user','logout']); ?>" tabindex="-1" aria-disabled="true">Logout</a>

    </nav>
   
 <form action="">
 <div class="form-group">
                  
                  <?php
                    echo "<a class='btn__danger' href='".base_url()."user/principalfriend'>Volver</a>";
                     ?>
               
              </div>
 </form>

        <div class="conten_tabla">  
    <table > 
      <tbody>
        <tr class="head">

          <td>Foto</td>
          <td>Fecha</td>
        
        </tr>
        <?php
           
           if($photographs!==null){
          foreach ($photographs as $photo) {
            //background="/dashboard/mytree/uploads/Lluluchwan.full.2255997.png"
        echo "<td><img src='/dashboard/mytree/{$photo->profilePic}' width='200' height='200'> </td><td>{$photo->fechap}</td></tr>";
          
          }
        }
        ?>
     
      
      </tbody>
    </table>
    </div>
  </form>
    
    
 
    
  </body>
  </html>
