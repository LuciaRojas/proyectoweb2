<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($this->session->user->user_type=="friend"){
  $isAdmin = true;



}else{
  $isAdmin = false;

}

if(!$isAdmin){
  redirect(site_url(['user','mainprin']));

}
?>

<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/dashboard/mytree/css/style.css">
    <title>Editar</title>
  </head>
  <body>
  <div class="container">
    <div class="msg">
    <?php echo $this->session->flashdata('validation_errors');?>
    </div>
    <nav class="">
      <a  href="<?php echo site_url(['user','logout']); ?>" tabindex="-1" aria-disabled="true">Logout</a>

    </nav>
    <div class="contenedor">
            <h2>Insert Tree</h2>
            <form action="/dashboard/mytree/tree/treeinsert" method="POST" >

                <div class="form-group">
              
            
                </div class="form-group">
                <div>
            
                </div>
                <div class="form-group">
                <input type="hidden" name="id_tree" value=" ">
                </div>
                <div class="form-group">
                <input type="text"  id="" name="nom_tree" placeholder="Name Tree"  class="input__text" value="" require>
                <input type="number"  id="" name="monto" placeholder="Rode to donate"  class="input__text" value="" require>
                </div>
                <div class="form-group">
                    <select  id="id_especie" name="id_especie" class="input__text" require>
                    <option value="" disabled selected >Select Especie</option>
  
                        <?php
                        foreach ($especies as $especie) {
                        ?>
                            <option value="<?php echo $especie->id_especie;?>" selected="true"><?php echo $especie->especie;?></option>
                            <?php 
                        }
                        ?>
                        </select>
                </div>
                <div class="form-group">
                    <button type="submit" name="guardar" class="btn__primary">Save</button>
                    
                 
                </div>
                <div class="form-group">
                  
                    <?php
                      echo "<a class='btn__danger' href='".base_url()."user/principalfriend'>Volver</a>";
                       ?>
                 
                </div>

        </form>
  </div>
    
 
    
  </body>
  </html>
