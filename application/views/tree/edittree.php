<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($this->session->user->user_type=="Admin"){
  $isAdmin = true;



}else{
  $isAdmin = false;

}

if(!$isAdmin){
  redirect(site_url(['user','mainprin']));

}


?>

<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/dashboard/mytree/css/style.css">
    <title>Editar</title>

    <script>
      function ponerNombre(){
        var nom = document.getElementById("picture").value;

        var filename = nom.replace(/^.*[\\\/]/, '');

        document.getElementById("nom_arbol").value = filename;
      }
    </script>
  </head>
  <body>
  <div class="container">
    <div class="msg">
    <?php echo $this->session->flashdata('validation_errors');?>
    </div>
    <nav class="">
      <a  href="<?php echo site_url(['user','logout']); ?>" tabindex="-1" aria-disabled="true">Logout</a>

    </nav>
   
    <form action="/dashboard/mytree/tree/editarbolfoto" method="POST" enctype="multipart/form-data" >
  
        <div class="form-group">
        <button type="submit" name="guardar" class="btn__primary" onclick="ponerNombre()">Save</button>
        <input type="hidden"  id="nom_arbol" name="nom_arbol" class="input__text" value="">
      
        </div class="form-group">
        <div>
       
        </div>
        <div class="form-group">
        <input type="hidden" name="id_tree" value="<?php $id_tree = json_decode($edittree[0]->id_tree,true); echo $id_tree; ?>">
        <input type="hidden" name="id_user" value="<?php $id_user = json_decode($edittree[0]->id_user,true); echo $id_user; ?>">
        </div>
        <div class="form-group">
        <input type="text"  id="" name="alt" placeholder="Altura"  class="input__text" value="<?php $altura = json_decode($edittree[0]->alt,true); echo $altura; ?>" require>
        </div>
        <div class="form-group">
        <select  id="id_especie" name="id_especie" class="input__text" require>
        <option value="" disabled selected >Select Especie</option>
  
        <?php


$id_especie = json_decode($edittree[0]->id_especie,true);echo $id_especie;
          foreach ($especies as $especie) {
          
            if( $especie->id_especie==$id_especie){
           ?>
              <option  value="<?php echo $especie->id_especie;?>" selected="true"><?php echo $especie->especie;?></option>

           <?php
            }else{
           ?>
            <option value="<?php echo $especie->id_especie;?>" ><?php echo $especie->especie;?></option>
              <?php 
              }
          }
        ?>
        </select>
        </div>

        <div class="conten_tabla">  
       
    <table >
    <div class="form-group">
    <input type="file" name="picture" id="picture" require accept="image/*">
    </div>
    
      <tbody>
        <tr class="head">

          <td>Foto</td>
          <td>Fecha</td>
          <td>Actions</td>
        </tr>
        <?php
           
           if($photographs!==null){
          foreach ($photographs as $photo) {
            //background="/dashboard/mytree/uploads/Lluluchwan.full.2255997.png"
        echo "<td><img src='/dashboard/mytree/{$photo->profilePic}' width='200' height='200'> </td><td>{$photo->fechap}</td><td> <a class='btn__delete' href='".base_url()."tree/eliminarphoto/".$photo->profilePic.'/'.$photo->id_Photography.'/'.$photo->id_tree."'>Eliminar</a></td></tr>";
          
          }
        }
         
        ?>
     
      
      </tbody>
    </table>
    </div>


       <div class="form-group">
          
           <?php
           echo "<a class='btn__danger' href='".base_url()."tree/listtree/".$id_user."'>Volver</a>";
           ?>
        </div>
  </form>
    
    
 
    
  </body>
  </html>
