<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($this->session->user->user_type=="Admin"){
  $isAdmin = true;



}else{
  $isAdmin = false;

}

if(!$isAdmin){
  redirect(site_url(['user','mainprin']));

}
?>

<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/dashboard/mytree/css/style.css">
    <title>Document</title>
  </head>
  <body>
  <div class="container">
    <div class="msg">
    <?php echo $this->session->flashdata('error');?>
    </div>
    <nav >
      <a  href="<?php echo site_url(['user','logout']); ?>" tabindex="-1" aria-disabled="true">Logout</a>

    </nav>

  <form action="" class="formulario">
  <a href="../../user/dashboard"  class="btn__danger">Volver</a>
  </form>
    
    <div class="conten_tabla">  
    <table >
      <tbody>
      <tr class="head">
      

          <td>Nombre Árbol</td>
          <td>Altura (cm)</td>
          <td>Especie</td>
          <td>Actions</td>
        </tr>
        <?php 
     
   
     if($trees!==null){
      foreach ($trees as $tree) {                                                
        echo "<tr><td>{$tree->nom_tree}</td><td>{$tree->alt}</td><td>{$tree->especie}</td><td> <a class='btn__update'  href='".base_url()."tree/edittree/".$tree->id_tree.'/'.$tree->id_user."'>Árbol</a></td></tr>";
    

     }
    }
          

        
        
       ?>

      
      </tbody>
    </table>
    </div>
 
    
  </body>
  </html>
