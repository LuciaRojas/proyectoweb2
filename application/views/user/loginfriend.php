<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/dashboard/mytree/css/style.css">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <title>Document</title>
</head>
<body>
<div class="container">
    <div class="msg">
    <?php echo $this->session->flashdata('error');?>
    </div>

    <section>
        <h1>Login Amigo</h1>
        <form action="/dashboard/mytree/user/authenticatefriend" method="POST" class="form-inline" role="form">
        <input type="text" name="email" placeholder="Your email" class="usuario">
        <input type="password" name="password" placeholder="Your password" class="contra">
      
        <input type="submit" name="aceptar" value="Login" class="btnaceptLo"> <br>

        </form>
        <a href="sign_in">sign in?</a>
    </section>
</div>


</body>
</html>
